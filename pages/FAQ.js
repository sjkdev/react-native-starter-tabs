import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';
import Home from './pages/Home';
import About from './pages/About';
import FAQ from './pages/FAQ';
import Contact from './pages/Contact';

class FAQ extends Component {
    render() {
        <View style={styles.container}>
             <Button title="go back to Home screen" onPress={() => this.props.navigate('Home')} />
            <Button title="go back to About screen" onPress={() => this.props.navigate('About')} />
            <Button title="go back to FAQ screen" onPress={() => this.props.navigate('FAQ')} />
            <Button title="go back to Contact screen" onPress={() => this.props.navigate('Contact')} />
        </View>
    }
}

export default FAQ;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    }
  });